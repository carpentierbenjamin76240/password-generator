﻿namespace BCA_PasswordGenerator
{
    partial class FrmExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtNameExport = new System.Windows.Forms.TextBox();
            this.Btn_Export = new System.Windows.Forms.Button();
            this.SFD_Export = new System.Windows.Forms.SaveFileDialog();
            this.BtnChemin = new System.Windows.Forms.Button();
            this.FBDIALOG = new System.Windows.Forms.FolderBrowserDialog();
            this.RBtnTxt = new System.Windows.Forms.RadioButton();
            this.RBtnCSV = new System.Windows.Forms.RadioButton();
            this.RBtnHTML = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Format d\'exportation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nom d\'exportation par défaut";
            // 
            // TxtNameExport
            // 
            this.TxtNameExport.BackColor = System.Drawing.Color.AliceBlue;
            this.TxtNameExport.Location = new System.Drawing.Point(27, 113);
            this.TxtNameExport.Name = "TxtNameExport";
            this.TxtNameExport.Size = new System.Drawing.Size(100, 20);
            this.TxtNameExport.TabIndex = 5;
            // 
            // Btn_Export
            // 
            this.Btn_Export.Location = new System.Drawing.Point(96, 170);
            this.Btn_Export.Name = "Btn_Export";
            this.Btn_Export.Size = new System.Drawing.Size(75, 23);
            this.Btn_Export.TabIndex = 9;
            this.Btn_Export.Text = "Exporter";
            this.Btn_Export.UseVisualStyleBackColor = true;
            this.Btn_Export.Click += new System.EventHandler(this.Btn_Export_Click);
            // 
            // BtnChemin
            // 
            this.BtnChemin.Location = new System.Drawing.Point(143, 113);
            this.BtnChemin.Name = "BtnChemin";
            this.BtnChemin.Size = new System.Drawing.Size(143, 23);
            this.BtnChemin.TabIndex = 10;
            this.BtnChemin.Text = "Chemin d\'exportation";
            this.BtnChemin.UseVisualStyleBackColor = true;
            this.BtnChemin.Click += new System.EventHandler(this.BtnChemin_Click);
            // 
            // RBtnTxt
            // 
            this.RBtnTxt.AutoSize = true;
            this.RBtnTxt.Location = new System.Drawing.Point(12, 50);
            this.RBtnTxt.Name = "RBtnTxt";
            this.RBtnTxt.Size = new System.Drawing.Size(39, 17);
            this.RBtnTxt.TabIndex = 11;
            this.RBtnTxt.TabStop = true;
            this.RBtnTxt.Text = ".txt";
            this.RBtnTxt.UseVisualStyleBackColor = true;
            // 
            // RBtnCSV
            // 
            this.RBtnCSV.AutoSize = true;
            this.RBtnCSV.Location = new System.Drawing.Point(71, 50);
            this.RBtnCSV.Name = "RBtnCSV";
            this.RBtnCSV.Size = new System.Drawing.Size(45, 17);
            this.RBtnCSV.TabIndex = 12;
            this.RBtnCSV.TabStop = true;
            this.RBtnCSV.Text = ".csv";
            this.RBtnCSV.UseVisualStyleBackColor = true;
            // 
            // RBtnHTML
            // 
            this.RBtnHTML.AutoSize = true;
            this.RBtnHTML.Location = new System.Drawing.Point(143, 50);
            this.RBtnHTML.Name = "RBtnHTML";
            this.RBtnHTML.Size = new System.Drawing.Size(47, 17);
            this.RBtnHTML.TabIndex = 13;
            this.RBtnHTML.TabStop = true;
            this.RBtnHTML.Text = ".html";
            this.RBtnHTML.UseVisualStyleBackColor = true;
            // 
            // FrmExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(298, 208);
            this.Controls.Add(this.RBtnHTML);
            this.Controls.Add(this.RBtnCSV);
            this.Controls.Add(this.RBtnTxt);
            this.Controls.Add(this.BtnChemin);
            this.Controls.Add(this.Btn_Export);
            this.Controls.Add(this.TxtNameExport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmExport";
            this.Text = "Exportation des données";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtNameExport;
        private System.Windows.Forms.Button Btn_Export;
        private System.Windows.Forms.SaveFileDialog SFD_Export;
        private System.Windows.Forms.Button BtnChemin;
        private System.Windows.Forms.FolderBrowserDialog FBDIALOG;
        private System.Windows.Forms.RadioButton RBtnTxt;
        private System.Windows.Forms.RadioButton RBtnCSV;
        private System.Windows.Forms.RadioButton RBtnHTML;
    }
}