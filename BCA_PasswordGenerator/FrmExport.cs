﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using BCA_Utils;

namespace BCA_PasswordGenerator
{
    public partial class FrmExport : Form
    {
        //ExportCSV.ExportCSV myExport = new ExportCSV.ExportCSV();
        BCA_PATH MonNomDeFichier = new BCA_PATH();
        SqlConnection gObjetConnection = new SqlConnection();
        String strRequete;
        SqlCommand command;
        BCA_CRYPTPWD crypt = new BCA_CRYPTPWD();

        private void MaConnexion()
        {
            // La propriété ConnectionString de notre objet gObjetConnection doit contenir les paramètres de connexion au serveur
            gObjetConnection.ConnectionString = "User ID = " + DonnéesPubliques.myValuelogin + ";PWD= " + DonnéesPubliques.myValueMpd + " ; Server= " + DonnéesPubliques.myValueServeur + "; Database= " + DonnéesPubliques.myValueBD + "; ";
            try //Tentative (try) d'ouverture d'un connexion avec le serveur de base de données
            {
                // Oyvrir la connexion en utilisant la méthode open() sur notre objet gObjetConnection
                gObjetConnection.Open();
            }
            catch (System.Data.SqlClient.SqlException probleme)
            {
                MessageBox.Show("Impossible de se connecter au serveur de base de données." + probleme.Message);
            }
        }
        private void MaDeconnexion()
        {
            gObjetConnection.Close();
        }

        public FrmExport()
        {
            InitializeComponent();
        }


       

        private void BtnChemin_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog Fbd = new FolderBrowserDialog();
            Fbd.RootFolder = Environment.SpecialFolder.Desktop;
            Fbd.Description = "Sélectionnez le dossier de sortie de vos mots de passe";
            Fbd.ShowNewFolderButton = true;
            

            if (Fbd.ShowDialog() == DialogResult.OK)
            {
                BtnChemin.Text = Fbd.SelectedPath;
            }
        }
        
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            #region Drapeaux
            Boolean ValidFormat, ValidChemin, ValidName, ValidFileExist, ValidDataDel;
            ValidChemin = false;
            ValidDataDel = false;
            ValidFileExist = true;
            ValidFormat = false;
            ValidName = true; 
            #endregion

            String NomFichier = MonNomDeFichier.RemoveInvalidChars(TxtNameExport.Text);
            String file = BtnChemin.Text + "\\" + NomFichier;
            if (File.Exists(file))
            {
                ValidFileExist = false;
                MessageBox.Show("Il existe un fichier portant le même nom que celui que vous essayez d\'exporter, veuillez changer le chemin d\'exportation ou le nom du fichier.", "Erreur d\'exportation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (String.IsNullOrEmpty(TxtNameExport.Text))
            {
                ValidName = false;
            }

            #region Format CSV
            if (RBtnCSV.Checked && ValidFileExist)
            {
                String Format;
                bool SaveData;
                List<String> mail = new List<String>();
                List<String> password = new List<String>();
                int i = 0;
                Format = BtnChemin.Text;
                MaConnexion();
                strRequete = "SELECT Libelle,PWD FROM PWD where idLog = " + DonnéesPubliques.IDLOGIN;
                //Tentative (Try) d'xécution de la requête sur la base de données correspondante à notre connexion
                try
                {// Instanciation de l'objet command en passent comme argument au construteur la requête ainsi que la connexion
                    command = new SqlCommand(strRequete, gObjetConnection);
                    // Déclaration d'un tableau mémoire en lecture uniquement destiné à recevoir le résultat de la requête
                    //(utilisation de la classe SqlDataReader)
                    SqlDataReader SdrListe;
                    // affectation de ce tableau (avec le résultat la requête)
                    // en utilisant la méthode ExecuteReader() sur notre objet command
                    // car plusieurs enregistrements sont renvoyés
                    SdrListe = command.ExecuteReader();
                    // Parcours du tableau mémoire (SqlDataReader) Tant qu'il existe une ligne à lire
                    while (SdrListe.Read())
                    {
                        // Ajout dans la datagridview d'une ligne correspondant à la ligne lue dans le tableau mémoire

                        mail.Add(Convert.ToString(SdrListe["Libelle"]) + "|" + crypt.DECRYPT(Convert.ToString(SdrListe["PWD"])));
                    }
                    //Fermeture du DataReader
                    SdrListe.Close();
                }
                catch (System.Data.SqlClient.SqlException probleme)
                {
                    MessageBox.Show("L'erreur suivante a été rencontrée :" + probleme.Message);
                }
                MaDeconnexion(); //appel de la procédure de Déconnexion
                if (BtnChemin.Text == "Chemin d'exportation")
                {
                    BtnChemin.Text = @"C:\Password\";
                }
                String chemin = BtnChemin.Text + "\\";
                StreamWriter File = new StreamWriter(chemin + NomFichier + ".csv");
                foreach (String m in mail)
                {
                    File.Write("\n" + m.ToString());
                }

                File.Close();
                MessageBox.Show("Votre exportation de vos mot de passe à réussi.");
            }
            #endregion

            #region Format TXT
            if (RBtnTxt.Checked && ValidFileExist)
            {
                String Format;
                bool SaveData;
                List<String> mail = new List<String>();
                List<String> password = new List<String>();
                int i = 0;
                Format = BtnChemin.Text;
                MaConnexion();
                strRequete = "SELECT Libelle,PWD FROM PWD where idLog = " + DonnéesPubliques.IDLOGIN;
                //Tentative (Try) d'xécution de la requête sur la base de données correspondante à notre connexion
                try
                {// Instanciation de l'objet command en passent comme argument au construteur la requête ainsi que la connexion
                    command = new SqlCommand(strRequete, gObjetConnection);
                    // Déclaration d'un tableau mémoire en lecture uniquement destiné à recevoir le résultat de la requête
                    //(utilisation de la classe SqlDataReader)
                    SqlDataReader SdrListe;
                    // affectation de ce tableau (avec le résultat la requête)
                    // en utilisant la méthode ExecuteReader() sur notre objet command
                    // car plusieurs enregistrements sont renvoyés
                    SdrListe = command.ExecuteReader();
                    // Parcours du tableau mémoire (SqlDataReader) Tant qu'il existe une ligne à lire
                    while (SdrListe.Read())
                    {
                        // Ajout dans la datagridview d'une ligne correspondant à la ligne lue dans le tableau mémoire

                        mail.Add(Convert.ToString(SdrListe["Libelle"]) + "|" + crypt.DECRYPT(Convert.ToString(SdrListe["PWD"])));
                    }
                    //Fermeture du DataReader
                    SdrListe.Close();
                }
                catch (System.Data.SqlClient.SqlException probleme)
                {
                    MessageBox.Show("L'erreur suivante a été rencontrée :" + probleme.Message);
                }
                MaDeconnexion(); //appel de la procédure de Déconnexion
                if (BtnChemin.Text == "Chemin d'exportation")
                {
                    BtnChemin.Text = @"C:\Password\";
                }
                String chemin = BtnChemin.Text + "\\";
                StreamWriter File = new StreamWriter(chemin + NomFichier + ".txt");
                foreach (String m in mail)
                {
                    File.Write("\n" + m.ToString());
                }

                File.Close();
                MessageBox.Show("Votre exportation de vos mots de passe à réussi.");
            }
            #endregion

            #region Format HTML
            if (RBtnHTML.Checked && ValidFileExist)
            {
                String Format;
                bool SaveData;
                List<String> mail = new List<String>();
                List<String> password = new List<String>();
                int i = 0;
                Format = BtnChemin.Text;
                MaConnexion();
                strRequete = "SELECT Libelle,PWD FROM PWD where idLog = " + DonnéesPubliques.IDLOGIN;
                //Tentative (Try) d'xécution de la requête sur la base de données correspondante à notre connexion
                try
                {// Instanciation de l'objet command en passent comme argument au construteur la requête ainsi que la connexion
                    command = new SqlCommand(strRequete, gObjetConnection);
                    // Déclaration d'un tableau mémoire en lecture uniquement destiné à recevoir le résultat de la requête
                    //(utilisation de la classe SqlDataReader)
                    SqlDataReader SdrListe;
                    // affectation de ce tableau (avec le résultat la requête)
                    // en utilisant la méthode ExecuteReader() sur notre objet command
                    // car plusieurs enregistrements sont renvoyés
                    SdrListe = command.ExecuteReader();
                    // Parcours du tableau mémoire (SqlDataReader) Tant qu'il existe une ligne à lire
                    while (SdrListe.Read())
                    {
                        // Ajout dans la datagridview d'une ligne correspondant à la ligne lue dans le tableau mémoire

                        mail.Add(Convert.ToString(SdrListe["Libelle"]) + "|" + crypt.DECRYPT(Convert.ToString(SdrListe["PWD"])));
                    }
                    //Fermeture du DataReader
                    SdrListe.Close();
                }
                catch (System.Data.SqlClient.SqlException probleme)
                {
                    MessageBox.Show("L'erreur suivante a été rencontrée :" + probleme.Message);
                }
                MaDeconnexion(); //appel de la procédure de Déconnexion
                if (BtnChemin.Text == "Chemin d'exportation")
                {
                    BtnChemin.Text = @"C:\Password\";
                }
                String chemin = BtnChemin.Text + "\\";
                StreamWriter File = new StreamWriter(chemin + NomFichier + ".html");
                File.Write("<h1> Exportation des mots de passe de " + DonnéesPubliques.NOM + " contenant " + DonnéesPubliques.NBPWD + " mot(s) de passe.</h1>");
                File.Write("<br><h5> Mot de passe : </h5>");
                File.Write("<table style=\"border: 1px solid black; border-collapse: collapse\"><tr><th style=\"border: 1px solid black\">Libellé</th><th style=\"border: 1px solid black\">Mot de passe</th></tr>");
                foreach (String m in mail)
                {
                    String[] decoupe = m.Split('|');
                    String libelle = decoupe[0];
                    String passwd = decoupe[1];
                    File.Write("<tr><td style=\"border: 1px solid black\">" + libelle+ "</td><td style=\"border: 1px solid black\">" + passwd+"</td></tr>");

                }
                File.Write("</table>");
                File.Close();
                MessageBox.Show("Votre exportation de vos mots de passe à réussi.");       
                RBtnHTML.Checked = false;
            } 
            #endregion

        }
    }
}
