# GenPWD

Un générateur de mot de passe avec système de compte. Permet de générer un ou plusieurs mots de passe et les afficher ensuite.
- Système de chiffrement des mots de passe personnalisés.  
Description détaillé sur mon [Portfolio](https://cpntben.wordpress.com). 

## License
[MIT](https://choosealicense.com/licenses/mit/)
